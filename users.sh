#!/bin/bash
#Purpose: print warning after the logged in users threshold met..more than one user log into one system
LOGGED_USERS=`uptime | awk {' print $6'}`
if [ $LOGGED_USERS -gt 4 ]
then
	echo "critical, logged in users = $LOGGED_USERS"
elif [ $LOGGED_USERS -gt 2 ]
then
	echo "warning, logged in users = $LOGGED_USERS"
else
	echo "ok, logged in users = $LOGGED_USERS"
fi
